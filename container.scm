(define-module (container)
  #:export (/containers/json
            /containers/create))


(use-modules (web client)
             (web uri)
             (json)
             (ice-9 iconv)
             (list-helpers)
             (rnrs enums)
             ((model) #:prefix model:)
             (url-helpers)
             (api-utils)
             (srfi srfi-1))


(define-api-route /containers/json GET application/x-www-form-urlencoded)
(define-api-route /containers/create POST application/json)


;; REF: https://docs.docker.com/engine/api/v1.38/#operation/ContainerList

;; (define* (/containers/json dock-sock #:key (data #f))
;;   (call-with-values
;;       (lambda ()
;;         (http-get "/containers/json"
;;                   #:port dock-sock
;;                   #:version '(1 . 1)
;;                   #:keep-alive? #f
;;                   #:headers '((host . ("localhost" . #f))
;;                               (content-type . (application/x-www-form-urlencoded (charset . "utf-8"))))
;;                   #:body (scm->json-string data)
;;                   #:decode-body? #t
;;                   #:streaming? #f))
;;     (lambda (response response-text)
;;       (let ([resp-text-as-string (bytevector->string response-text "utf-8")])
;;         (cons response resp-text-as-string)))))


;; (define* (/containers/create dock-sock data)
;;   (call-with-values
;;       (lambda ()
;;         (http-post "/containers/create"
;;                    #:port dock-sock
;;                    #:body (scm->json-string data)
;;                    ;; dockerd uses HTTP 1.1 it seems.
;;                    ;; other values will result in: "Bad Request: unsupported protocol version"
;;                    #:version '(1 . 1)
;;                    #:keep-alive? #f
;;                    ;; Apparently the `host` header must be specified.
;;                    ;; The `host` header in this case is ("localhost" . #f).
;;                    ;; The `host` header contains domain and port
;;                   #:headers '((host . ("localhost" . #f))
;;                               (content-type . (application/json (charset . "utf-8"))))
;;                    #:decode-body? #t
;;                    #:streaming? #f))
;;     (lambda (response response-text)
;;       (display
;;        (simple-format #f
;;                       "RESPONSE TEXT: ~s"
;;                       (bytevector->string response-text "utf-8")))
;;       (let ([resp-text-as-string (bytevector->string response-text "utf-8")])
;;         (cons response resp-text-as-string)))))
