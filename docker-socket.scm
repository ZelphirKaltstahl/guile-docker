(define-module (docker-socket)
  #:export (connect-to-docker-socket))

(use-modules (web client)
             (web uri)
             (ice-9 iconv))

(define* (connect-to-docker-socket #:key (socket-path "/var/run/docker.sock"))
  (let ([docker-sock-addr (make-socket-address AF_UNIX socket-path)]
        [docker-sock (socket PF_UNIX SOCK_STREAM 0)])
    ;; socket options:
    ;; https://www.gnu.org/software/libc/manual/html_node/Socket_002dLevel-Options.html
    (setsockopt docker-sock SOL_SOCKET SO_REUSEADDR 1)
    ;; usage of connect:
    ;; https://www.gnu.org/software/guile/manual/html_node/Network-Sockets-and-Communication.html#Network-Sockets-and-Communication
    ;; server side would use `bind`, `accept` and `listen`.
    ;; client side uses `connect` and `close`.
    (connect docker-sock docker-sock-addr)
    docker-sock))
