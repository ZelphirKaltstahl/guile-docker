(define-module (model)
  #:export (container-list-filter-keys))

(use-modules (rnrs enums))

(define container-list-filter-keys
  (make-enumeration '(ancestor
                      before
                      expose
                      exited
                      health
                      id
                      isolation
                      is-task
                      label
                      name
                      nework
                      publish
                      since
                      status
                      volume)))
