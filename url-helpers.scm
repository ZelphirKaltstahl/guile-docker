(define-module (url-helpers)
  #:export (make-complete-api-url
            scm-json->uri-encoded-string))

(use-modules (web client)
             (web uri)
             (json))


(define (make-complete-api-url api-route query-params-as-string)
  (string-append api-route "?" query-params-as-string))


(define (scm-json->uri-encoded-string scm-json)
  "The argument scm-json is the Scheme representation of guile-json
for JSON."
  (uri-encode (scm->json-string scm-json)))


#;(define (data->query-params data)
  (cond
   [(null? data)
    (display (simple-format #f "CASE: ~s\n" "null"))
    "}"]
   [(vector? data)
    (display (simple-format #f "CASE: ~s\n" "vector"))
    (string-append "["
                   (string-join (map data->query-params (vector->list data)) ",")
                   "]")]
   [(pair? data)
    (display (simple-format #f "CASE: ~s\n" "pair"))
    (cond
     [(string? (car data))
      (string-append (car data)
                     "="
                     (data->query-params (cdr data)))]
     ;; should be pair
     [else (string-append (data->query-params (car data))
                          "&"
                          (data->query-params (cdr data)))])]
   ;; should be primitive data
   [(number? data)
    (display (simple-format #f "CASE: ~s\n" "number"))
    (number->string data)]
   [(boolean? data)
    (display (simple-format #f "CASE: ~s\n" "boolean"))
    (if data "true" "false")]
   [else
    ;; should be string
    (string-append "" data "")]))
