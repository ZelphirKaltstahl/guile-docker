(define-module (list-helpers)
  #:export (conditionally-make-alist-of
            alist-to-query-params
            alist-keys))


(define (conditionally-make-alist-of names vals)
  (cond [(null? names) '()]
        [(car vals)
         (cons (cons (car names) (car vals))
               (conditionally-make-alist-of (cdr names) (cdr vals)))]
        [else (conditionally-make-alist-of (cdr names) (cdr vals))]))


(define (alist-to-query-params alist)
  (string-join (map (lambda (assoc)
                      (string-append (car assoc) "=" (cdr assoc)))
                    alist)
               "&"))


(define (alist-keys alist)
  (map car alist))
