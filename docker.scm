;; (Programming against the v1.38 interface of docker engine.)
(add-to-load-path (dirname (current-filename)))


(use-modules (json)
             (container)
             (docker-socket))

(let ([dock-sock (connect-to-docker-socket)])
  (let ([resp-and-resp-text
         (/containers/json dock-sock
                            #:data '(("all" . "true")
                                     ("filters" . (("name" . #("db"))
                                                   ("status" . #("running" "exited"))))))])
    (display resp-and-resp-text)
    (newline)))

(let ([dock-sock (connect-to-docker-socket)])
  (let ([resp-and-resp-text
         (/containers/create dock-sock
                             #:data '(("Image" . "ubuntu")
                                      ("Cmd" . #("date"))))])
    (display resp-and-resp-text)
    (newline)))
